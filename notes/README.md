- [1. Setup Project](#1-setup-project)
  - [Top-level folders](#top-level-folders)
  - [Creating Routes](#creating-routes)
- [2. Layout \& Template](#2-layout--template)
  - [Layouts](#layouts)
  - [Templates](#templates)
- [3. Group Routes \& Dynamic Routes](#3-group-routes--dynamic-routes)
  - [Route Groups (folderName)](#route-groups-foldername)
  - [Dynamic Routes (\[id\] \&\& `params` props)](#dynamic-routes-id--params-props)
- [4. Link \& Navigation](#4-link--navigation)
  - [ Component](#-component)
    - [`href` (required)](#href-required)
- [5. API Route Handlers](#5-api-route-handlers)
  - [Extended `NextRequest` and `NextResponse` APIs](#extended-nextrequest-and-nextresponse-apis)
    - [NextRequest](#nextrequest)
    - [NextResponse](#nextresponse)
- [6. Data Fetching](#6-data-fetching)
- [7. Caching \& Revalidating](#7-caching--revalidating)
  - [Caching Data](#caching-data)
    - [Opting out of Data Caching](#opting-out-of-data-caching)
    - [Individual fetch Requests](#individual-fetch-requests)
  - [Revalidating Data](#revalidating-data)
    - [Time-based Revalidation](#time-based-revalidation)
    - [On-demand Revalidation](#on-demand-revalidation)
- [8. Loading UI \& Error Handling](#8-loading-ui--error-handling)
  - [Loading UI](#loading-ui)
  - [Error Handling](#error-handling)
- [9. Middleware](#9-middleware)
  - [`matcher` concept](#matcher-concept)
- [10. Parallel Routes](#10-parallel-routes)
  - [Slots](#slots)
    - [default.ts](#defaultts)
- [11. Intercepting Route](#11-intercepting-route)
  - [Convention (cara tulis)](#convention-cara-tulis)
  - [Examples](#examples)
    - [Modals](#modals)
- [12. Connect Firebase DB](#12-connect-firebase-db)
- [13. Setup Next-Auth (PENTING)](#13-setup-next-auth-penting)
  - [Install NextAuth](#install-nextauth)
    - [useSession() from "next-auth/react"](#usesession-from-next-authreact)
    - [Credentials](#credentials)

# 1. Setup Project

> Next.js Project Structure: https://nextjs.org/docs/getting-started/project-structure

## Top-level folders

- `app`: App Router (`app` directory purely for routing purposes)
- `pages`: Pages Router
- `public`: Static assets to be served
- `src`: Optional application source folder

Kebiasaan folder yg ada:

- app
- components
- lib
- services

## Creating Routes

> https://nextjs.org/docs/app/building-your-application/routing/defining-routes#creating-routes

# 2. Layout & Template

> Pages and Layouts: https://nextjs.org/docs/app/building-your-application/routing/pages-and-layouts
> kalau pakai `layout`, state-nya di bawa ke route page2 lain. kalau `template` state tak dibawa.

## Layouts

- Page: A page is UI that is unique to a route.define pages by exporting a component from a `page.js` file.

- Layouts: A layout is UI that is **shared** between multiple pages. On navigation, layouts preserve state, remain interactive, and do not re-render. Layouts can also be nested.

- first start dari layout.tsx -> access `{children}`, iaitu bergantung pd page yg kita buka (routing mana yg kita buka cth root `page.tsx`) -> then go to `page.tsx`

- "jika disableNavbar TIDAK(!) mengandungi pathname iaitu '/login' dgn '/register', maka paparkan elemen Navbar"

```typescript
{
  !disableNavbar.includes(pathname) && <Navbar />;
}
```

- kalau bila berada di page url '/login' dgn '/register', jadi takda Navbar

## Templates

> Templates: https://nextjs.org/docs/app/building-your-application/routing/pages-and-layouts#templates
> template akan berubah kalau taksama

- Templates: Templates are similar to layouts in that they wrap each child layout or page.

- templates create a new instance for each of their children on navigation.

- templates would be a more suitable option than layouts. For example:

- Features that rely on `useEffect` (e.g logging page views) and `useState` (e.g a per-page feedback form).

- To change the default framework behavior. For example, Suspense Boundaries inside layouts only show the fallback the first time the Layout is loaded and not when switching pages. For templates, the fallback is shown on each navigation.

> when go to different route, template will change

# 3. Group Routes & Dynamic Routes

> https://nextjs.org/docs/app/building-your-application/routing/route-groups

## Route Groups (folderName)

- A route group can be created by wrapping a folder's name in parenthesis: `(folderName)`.
- The **folders in parenthesis will be omitted from the URL** (e.g. `(marketing)` or `(shop)`).

```
src/
└── app/
    └── product/
    └── (auth)
        └── login/
            └── page.tsx
        └── register/
            └── page.tsx
    └── (admin)
        └── dashboard/
            └── [id]/
                └── page.tsx
        └── user/
            └── (.)detail/
                └── [id]/
                    └── page.tsx
            └── page.tsx
```

## Dynamic Routes ([id] && `params` props)

> https://nextjs.org/docs/app/building-your-application/routing/dynamic-routes

- **When don't know the exact segment names ahead of time** and want to create routes from dynamic data, you can use Dynamic Segments that are **filled in at request time or prerendered at build time**.

- A Dynamic Segment can be created by wrapping a folder's name in square brackets: `[folderName]`. For example, `[id]` or `[slug]`.

- Dynamic Segments are **passed as the `params` prop** to layout, page, route, and generateMetadata functions.

# 4. Link & Navigation

> https://nextjs.org/docs/app/building-your-application/routing/linking-and-navigating

here are three ways to navigate between routes in Next.js:

- Using the [`<Link>` Component](https://nextjs.org/docs/app/building-your-application/routing/linking-and-navigating#link-component)
- Using the [`useRouter` Hook](https://nextjs.org/docs/app/building-your-application/routing/linking-and-navigating#userouter-hook)
- Using the native [History API](https://nextjs.org/docs/app/building-your-application/routing/linking-and-navigating#using-the-native-history-api)

## <Link> Component

> other optional props you can pass to <Link>. See the [API reference](https://nextjs.org/docs/app/api-reference/components/link) for more.

| Prop     | Example           | Type             | Required |
| -------- | ----------------- | ---------------- | -------- |
| href     | href="/dashboard" | String or Object | Yes      |
| replace  | replace={false}   | Boolean          | -        |
| scroll   | scroll={false}    | Boolean          | -        |
| prefetch | prefetch={false}  | Boolean          | -        |

### `href` (required)

`href` can also accept an object, for example:

```typescript
// Navigate to /about?name=test
<Link
  href={{
    pathname: "/about",
    query: { name: "test" },
  }}
>
  About
</Link>
```

**Functions that common in Link & Navigation:**

1. [`usePathname`](https://nextjs.org/docs/app/api-reference/functions/use-pathname): a **Client Component** hook that lets you read the **current URL's pathname**.

| URL               | Returned value      |
| ----------------- | ------------------- |
| /                 | '/'                 |
| /dashboard        | '/dashboard'        |
| /dashboard?v=2    | '/dashboard'        |
| /blog/hello-world | '/blog/hello-world' |

2. [`useRouter`](https://nextjs.org/docs/app/api-reference/functions/use-router): allows you to programmatically change routes inside Client Components. [👉 read this](https://nextjs.org/docs/app/api-reference/functions/use-router#userouter)

> **Recommendation**: Use the <Link> component for navigation unless you have a specific requirement for using useRouter.

Certainly, here's the information presented in a clear format:

- **`router.push(href: string, { scroll: boolean })`**: Perform a client-side navigation to the provided route. Adds a new entry into the browser’s history stack.

- **`router.replace(href: string, { scroll: boolean })`**: Perform a client-side navigation to the provided route without adding a new entry into the browser’s history stack.

- **`router.refresh()`**: Refresh the current route. Makes a new request to the server, re-fetching data requests, and re-rendering Server Components. The client will merge the updated React Server Component payload without losing unaffected client-side React (e.g., useState) or browser state (e.g., scroll position).

- **`router.prefetch(href: string)`**: Prefetch the provided route for faster client-side transitions.

- **`router.back()`**: Navigate back to the previous route in the browser’s history stack.

- **`router.forward()`**: Navigate forwards to the next page in the browser’s history stack.

3. [`useSearchParams`](https://nextjs.org/docs/app/api-reference/functions/use-search-params): is a **Client Component** hook that lets you read the current URL's query string. returns a **read-only** version

```typescript
"use client";

import { useSearchParams } from "next/navigation";

export default function SearchBar() {
  const searchParams = useSearchParams();

  const search = searchParams.get("search");

  // URL -> `/dashboard?search=my-project`
  // `search` -> 'my-project'
  // benda yg kita nk search itulah query
  return <>Search: {search}</>;
}
```

# 5. API Route Handlers

> https://nextjs.org/docs/app/building-your-application/routing/route-handlers
> kalau API,function-nya pakai `async await`
> tgk folder /(auth)/login/page.tsx dgn /api/auth/login/route.ts

- Allow you to **create custom request handlers** for a given route using the Web Request and Response APIs.
- Route Handlers are defined in a `route.js|ts` file inside the `app` directory:

## Extended `NextRequest` and `NextResponse` APIs

> https://nextjs.org/docs/app/building-your-application/routing/route-handlers#extended-nextrequest-and-nextresponse-apis

### NextRequest

> https://nextjs.org/docs/app/api-reference/functions/next-request > `Request` mdn web docs: https://developer.mozilla.org/en-US/docs/Web/API/Request
> "Request" is associated with the client

```code
-On this page-

cookies
  set(name, value)
  get(name)
  getAll()
  delete(name)
  has(name)
  clear()
nextUrl❗️
ip
geo
```

- [`nextUrl`](https://nextjs.org/docs/app/api-reference/functions/next-request#nexturl): Extends the native URL API with additional convenience methods, including Next.js specific properties.

```typescript
// Given a request to /home, pathname is /home
request.nextUrl.pathname;
// Apabila terdapat permintaan ke /home, nilai `pathname` akan menjadi /home

// Given a request to /home?name=lee, searchParams is { 'name': 'lee' }
request.nextUrl.searchParams;
// Apabila terdapat permintaan ke /home?name=lee, nilai `searchParams` akan menjadi { 'name': 'lee' }
// searchParams ni query mcm useSearchParams
```

### NextResponse

> https://nextjs.org/docs/app/api-reference/functions/next-response
> Response mdn web docs: https://developer.mozilla.org/en-US/docs/Web/API/Response
> "Response" is associated with the server

- NextResponse (json()): Produce a response with the given JSON body -> https://nextjs.org/docs/app/api-reference/functions/next-response#json

- go url: http://localhost:3000/api

```code
-On this page-

cookies
  set(name, value)
  get(name)
  getAll()
  delete(name)
json()❗️
redirect()❗️
rewrite()❗️
next()❗️
```

# 6. Data Fetching

- kalau API dari api route sendiri, bila map data, kita tambah `data`
- kalau api dari luar, mungkin takda. tgk API structure

contoh:

```typescript
{products.data.length > 0 && products.data.map((product: any) => {
```

```typescript
// API dari luar, tapi kalau amik API dari api route sendiri?
const res = await fetch("https://fakestoreapi.com/products", {
  cache: "no-store",
});

// API dari /api/product/route.ts
// const res = await fetch("http://localhost:3000/api/product", {
//   cache: "force-cache", // https://developer.mozilla.org/en-US/docs/Web/API/Request/cache
//   next: {
//     // revalidate: 30, // update setiap 30 saat. fungsi: utk performance (e.g. for analytics)
//     tags: ["products"],
//   },
// });
```

# 7. Caching & Revalidating

> cache: https://developer.mozilla.org/en-US/docs/Web/API/Request/cache

## Caching Data

### Opting out of Data Caching

> `fetch` requests are not cached if: https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating#opting-out-of-data-caching

`fetch` requests are not cached if:

- The `cache: 'no-store'` is added to `fetch` requests.
- The `revalidate: 0` option is added to individual `fetch` requests.
- The `fetch` request is inside a Router Handler that uses the `POST` method.
- The `fetch` request comes after the usage of `headers` or `cookies`.
- The `const dynamic = 'force-dynamic'` route segment option is used.
- The `fetchCache` route segment option is configured to skip cache by default.
- The `fetch` request uses `Authorization` or `Cookie` headers and there's an uncached request above it in the component tree.

### Individual fetch Requests

To opt out of caching for individual fetch requests, you can set the cache option in fetch to 'no-store'. This will fetch data dynamically, on every request.

## Revalidating Data

### Time-based Revalidation

> Time-based: https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating#time-based-revalidation

```typescript
// type CatchAllPageProps = { params: { slug: string[] } };
type ProductPageProps = { params: { slug: string[] } };

async function getData() {
  // API dari luar, tapi kalau amik API dari api route sendiri?
  // API dari /api/product/route.ts
  const res = await fetch("http://localhost:3000/api/product", {
    cache: "force-cache", // https://developer.mozilla.org/en-US/docs/Web/API/Request/cache
    next: {
      revalidate: 30, // update setiap 30 saat. fungsi: utk performance (e.g. for analytics)
    },
  });

  console.log("response fakestore api:", res);

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }
  return res.json();
}
```

### On-demand Revalidation

> on-demand: https://nextjs.org/docs/app/building-your-application/data-fetching/fetching-caching-and-revalidating#on-demand-revalidation

- validate secara manual
- ada `tags: [""]`, contoh `["products"]`
- buat folder kat /api folder `revalidate` folder, then create `route.ts` file
- `import { revalidateTag } from 'next/cache's`
- open POSTMAN/Thunder Client: POST `http://localhost:3000/api/revalidate?tag=products`

= Response:

```log
{
  "revalidate": true,
  "now": 1706241480972
}
```

```typescript
// type CatchAllPageProps = { params: { slug: string[] } };
type ProductPageProps = { params: { slug: string[] } };

async function getData() {
  // API dari luar, tapi kalau amik API dari api route sendiri?
  // const res = await fetch("https://fakestoreapi.com/products");

  // API dari /api/product/route.ts
  const res = await fetch("http://localhost:3000/api/product", {
    cache: "force-cache", // https://developer.mozilla.org/en-US/docs/Web/API/Request/cache
    next: {
      tags: ["products"],
    },
  });

  console.log("response fakestore api:", res);

  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }
  return res.json();
}
```

trycatch() method for error handling

```typescript
async function getData() {
  try {
    const res = await fetch("http://localhost:3000/api/product", {
      cache: "force-cache",
      next: {
        tags: ["products"],
      },
    });

    console.log("Response status:", res.status);

    if (!res.ok) {
      // console.error("Failed to fetch data:", res.statusText);
      throw new Error("Failed to fetch data");
    }

    const jsonData = await res.json();
    console.log("JSON Data:", jsonData);

    return jsonData;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
}
```

# 8. Loading UI & Error Handling

## Loading UI

> Loading UI and Streaming: https://nextjs.org/docs/app/building-your-application/routing/loading-ui-and-streaming

- hanya masukkan `loading.tsx` kat folder tempat yg kita nak load.
- nextjs akan buatkan loadnya

```typescript
cache: "force-cache"; // takda Loading... sbb kita dah store data (cache)

cache: "no-store"; // ada Loading...
```

## Error Handling

> Error Handling https://nextjs.org/docs/app/building-your-application/routing/error-handling

- hanya buat file `error.tsx` sama dgn loading ui
- tambah 's' kat products url -> `await fetch("https://fakestoreapi.com/products`
- error jadi neat

# 9. Middleware

> https://nextjs.org/docs/app/building-your-application/routing/middleware
> bila buka url cth '/about' kita terus redirect ke url '/'

- Middleware runs before cached content and routes are matched

```typescript
import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export function middleware(request: NextRequest) {
  // bila request/buka url cth '/about' kita terus redirect ke url '/'
  if (request.nextUrl.pathname.startsWith("/about")) {
    return NextResponse.rewrite(new URL("/", request.url));
  }

  if (request.nextUrl.pathname.startsWith("/dashboard")) {
    return NextResponse.rewrite(new URL("/dashboard/user", request.url));
  }
}
```

- `redirect` the incoming request to a different URL
- `rewrite` the response by displaying a given URL

- cth; kalau nak buat lagi utk URL lain guna conditional statements kena duplicate lagi. ini jadi leceh sbb kena duplicate byk2 utk page yg lain2. utk atasi, guna konsep `matcher`.
- jadi kita buat satu aturan supaya nanti URL mana yg akan meng-execute middleware

```typescript
import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export function middleware(request: NextRequest) {
  // bila request/buka url cth '/about' kita terus redirect ke url '/'
  // cth, kalau belum login akan di redirect ke '/login' page
  const isLogin = false;
  if (request.nextUrl.pathname.startsWith("/about")) {
    // belum login redirect ke login page
    if (!isLogin) {
      return NextResponse.redirect(new URL("/login", request.url));
    }
  }

  if (request.nextUrl.pathname.startsWith("/dashboard")) {
    return NextResponse.rewrite(new URL("/dashboard/user", request.url));
  }
}
```

### `matcher` concept

> matcher allows to filter Middleware to run on specific paths

- cth; kalau nak buat lagi utk URL lain guna conditional statements kena duplicate lagi. ini jadi leceh sbb kena duplicate byk2 utk page yg lain2. utk atasi, guna konsep `matcher`.
- jadi kita buat satu aturan supaya nanti URL mana yg akan meng-execute middleware

```typescript
import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export function middleware(request: NextRequest) {
  const isLogin = false;
  if (!isLogin) {
    return NextResponse.redirect(new URL("/login", request.url));
  }
}

// cth, kalau nk ke dashboard kena login dulu
export const config = {
  matcher: ["/dashboard", "/about/:path*"],
};
```

# 10. Parallel Routes

> https://nextjs.org/docs/app/building-your-application/routing/parallel-routes

- allows you to simultaneously or conditionally render one or more pages within the same layout.
- useful for highly dynamic sections of an app, such as dashboards and feeds on social sites.

## Slots

> pass as props to **Parent Layout**
> kena `npm run dev` semula bila create slots baru.

- Parallel routes are created using named slots. Slots are defined with the `@folder` convention.
- For example, the following file structure defines two slots: `@analytics` and `@team`
- Slots are passed as props to the shared parent layout.
- Slots are not route segments and do not affect the URL structure.

```typescript
export default function Layout({
  children,
  team,
  analytics,
}: {
  children: React.ReactNode;
  analytics: React.ReactNode;
  team: React.ReactNode;
}) {
  return (
    <>
      {children}
      {team}
      {analytics}
    </>
  );
}
```

### default.ts

> https://nextjs.org/docs/app/building-your-application/routing/parallel-routes#active-state-and-navigation

- contoh: nak render `product` ke dalam `/app/dashboard` -> `@product`
- tapi kalau kat `/app/dashboard/@payments` takda `page.tsx`, bila pergi ke dashboard page akan error 404. jadi untuk elak, create `default.tsx` kat `@payments` folder, masukkan kod mcm bawah:

```typescript
export default function Default() {
  return null;
}
```

# 11. Intercepting Route

> https://nextjs.org/docs/app/building-your-application/routing/intercepting-routes
> source code to learn: https://github.com/vercel/nextgram || https://nextgram.vercel.app/

- allows you to load a route from another part of your application within the current layout.
- useful when you want to display the content of a route without the user switching to a different context.
- For example, when clicking on a photo in a feed, you can display the photo in a modal, overlaying the feed.
- For example, when clicking on a photo in a feed, you can display the photo in a modal, overlaying the feed.

## Convention (cara tulis)

> Note that the `(..)` convention is based on route segments, not the file-system.

- Intercepting routes can be defined with the `(..)` convention, which is similar to relative path convention `../` but for segments.

You can use:

- `(.)` to match segments on the **same level**
- `(..)` to match segments **one level above**
- `(..)(..)` to match segments **two levels above**
- `(...)` to match segments from the **root** `app` directory

For example, you can intercept the `photo` segment from within the `feed` segment by creating a `(..)photo` directory.

```
└── /app (root path)

├── /feed

| ├── /(..)photo 🔵

| | ├── [id]

| | ├── page.js

├── /photo 🔵

| ├── [id]

| ├── page.js

├── layout.js

├── page.js
```

## Examples

> https://nextjs.org/docs/app/building-your-application/routing/intercepting-routes#examples

### Modals

> https://nextjs.org/docs/app/building-your-application/routing/intercepting-routes#modals
> Youtube (Intercepting routes, modal, components, useRef, useRouter, MouseEventHandler): https://www.youtube.com/watch?v=Osaed1pQSpg&list=PLmF_zPV9ZcP2aYRuoEsMla5gqNjxP-V20&index=31&ab_channel=VIPCODESTUDIO

Intercepting Routes can be used together with [Parallel Routes](https://nextjs.org/docs/app/building-your-application/routing/parallel-routes) to create modals. This allows you to solve common challenges when building modals, such as:

- Making the modal content **shareable through a URL**.
- **Preserving context** when the page is refreshed, instead of closing the modal.
- **Closing the modal on backwards navigation** rather than going to the previous route.
- **Reopening the modal on forwards navigation**.

Consider the following UI pattern, where a user can open a photo modal from a gallery using client-side navigation, or navigate to the photo page directly from a shareable URL:

![modals-nextjs14](https://nextjs.org/_next/image?url=%2Fdocs%2Fdark%2Fintercepted-routes-modal-example.png&w=1920&q=75&dpl=dpl_6Qir2sLFUSLWxJxpLbUBoZRU9wXc)

In the above example, the path to the `photo` segment can use the `(..)` matcher since `@modal` is a _slot_ and not a segment. This means that the `photo` route is only one segment level higher, despite being two _file-system_ levels higher.

- cth, kita nk create modal utk product detail dari product page. jadi create @modal folder didalam product folder. dalam @modal folder create lagi 1 folder utk detail page tapi utk modal. sbbkan detail folder 1 level keatas dari detail modal folder, jadi detail modal folder jadi `(.)detail` kat dalam @modal folder.

```
src/
└── app/
    └── api/
    └── product/
        └── detail/
            └── [id]/
                └── page.tsx
        └── @modal/
            └── (.)detail/
                └── [id]/
                    └── page.tsx
            └── default.tsx
        ├── layout.tsx
        ├── error.tsx
        ├── loading.tsx
        └── page.tsx
```

# 12. Connect Firebase DB

> https://www.youtube.com/watch?v=tS-0jvroAGA&list=PLmF_zPV9ZcP2aYRuoEsMla5gqNjxP-V20&index=32&ab_channel=VIPCODESTUDIO

# 13. Setup Next-Auth (PENTING)

> ❗️ Authentication: https://nextjs.org/docs/app/building-your-application/authentication
> Introduction: https://next-auth.js.org/getting-started/introduction

To implement authentication in Next.js, familiarize yourself with three foundational concepts:

- [Authentication](https://nextjs.org/docs/app/building-your-application/authentication#authentication) verifies if the user is who they say they are. It requires the user to prove their identity with something they have, such as a username and password.
- [Session Management](https://nextjs.org/docs/app/building-your-application/authentication#session-management) tracks the user's state (e.g. logged in) across multiple requests.
- [Authorization](https://nextjs.org/docs/app/building-your-application/authentication#authorization) decides what parts of the application the user is allowed to access.

## Install NextAuth

```code
npm install next-auth
```

- wrap root layout.tsx with session provider.
- pastu, `npm run dev`.

```typescript
// `/app/layout.tsx`
import { SessionProvider } from "next-auth/react";

<SessionProvider>
  {!disableNavbar.includes(pathname) && <Navbar />}

  {children}
</SessionProvider>;
```

- nak buatkan dashboard page hanya boleh akses bila dah login sbg admin. bukan utk publik.
- jadi boleh guna `useSession()`.

### useSession() from "next-auth/react"

- biasa kalau nk guna hooks atau context itu harus gunakan client (`"use client"`).
- useSession() untuk pengechekkan (menyemak).

> useSession() vs getServerSession()??

```typescript
// /app/(admin)/dashboard/page.tsx

"use client";

import { useSession } from "next-auth/react";
import { useEffect } from "react";
ß;
import { useRouter } from "next/navigation";

export default function DashboardPage() {
  const { data: session, status } = useSession();
  console.log(session); // sebab kita dh assign data sebagai session
  console.log(status);

  const router = useRouter();

  useEffect(() => {
    if (status === "unauthenticated") {
      router.push("/login");
    }
  }, [router, status]);

  return (
    <div className="w-full h-96 bg-gray-300 rounded-[12px] flex justify-center items-center">
      <h1>Dashboard</h1>
    </div>
  );
}
```

1. boleh delete `/app/api/auth/login` folder.
2. then, create a new folder [...nextauth] ex; `/app/api/auth/[...nextauth]`.
3. then create a new file route.ts `/app/api/auth/[...nextauth]/route.ts`. Dalam ni import `NextAuthOptions`

```typescript
import { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";

const authOptions: NextAuthOptions = {
  session: {
    strategy: "jwt",
  },
  secret: "e-hadir",
  providers: [
    CredentialsProvider({
      type: "credentials",
      name: "credentials",
      credentials: {
        email: {label: "Email", type: "email"},
        password: {label: "Password", type: "password"}
      }

      async authorize(credentials, req) {
        const {email, password} = credentials as {
          email: string,
          password: string
        };

        if (email === "amirah@gmail.com" && password === "12345") {
          return (
            id: 1,
            name: "Amirah Nazili",
            email: "amirah@gmail.com",
            role: "admin"
          )
        }
      }
    })
  ],
 callbacks: {
    async jwt({ token, account, profile, user }: any) {
      if (account?.provider === "credentials") {
        token.email = user.email;
        token.fullname = user.fullname;
        token.role = user.role;
      }
      return token;
    },

    // below async fn utk generate session
    async session({ session, token }: any) {
      if ("email" in token) {
        session.user.email = token.email;
      }
      if ("fullname" in token) {
        session.user.fullname = token.fullname;
      }
      if ("role" in token) {
        session.user.role = token.role;
      }

      return session;
    },
  },
};
```

1. pergi `/app/navbar.tsx` untuk edit Login button -> `onClick`.

```typescript
// buang
<button
  type="button"
  className="bg-white rounded-md px-2 text-sm h-6"
  onClick={() => router.push("/login")}
>
  Login
</button>

// edit jadi mcm ni
import { signIn } from "next-auth/react";

onClick={() => signIn()}
```

2. pergi semula ke `/app/api/auth/[...nextauth]/route.ts` utk tambah NextAuth

```typescript
import { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import NextAuth from "next-auth/next";

const authOptions: NextAuthOptions = {...}

const handler = NextAuth(authOptions);
export { handler as GET, handler as POST };
```

### Credentials

> https://next-auth.js.org/providers/credentials
