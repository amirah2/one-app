import { revalidateTag } from "next/cache";
import { NextRequest, NextResponse } from "next/server";

const malaysiaTime = new Date().toLocaleString("en-MY", {
  timeZone: "Asia/Kuala_Lumpur",
});

export async function POST(request: NextRequest) {
  // http://localhost:3000/api/revalidate?tag=products
  const tag = request.nextUrl.searchParams.get("tag");

  // http://localhost:3000/api/revalidate?tag=products&secret=12345678
  const secret = request.nextUrl.searchParams.get("secret");

  // secret simpan dalam .env
  if (secret !== process.env.REVALIDATE_TOKEN) {
    return NextResponse.json(
      { status: 401, message: "Invalid secret" },
      { status: 401 }
    );
  }

  if (!tag) {
    return NextResponse.json(
      { status: 400, message: "Missing tag param" },
      { status: 401 }
    );
  }

  revalidateTag(tag);

  return NextResponse.json({ revalidate: true, now: malaysiaTime }); // revalidate true dgn sekali timestamp-nya
}
