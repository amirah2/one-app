// HOME page

import Link from "next/link";

export default function HomePage() {
  return (
    <main>
      <div className="text-center p-5">
        <h1 className=" text-3xl">Hello, World!</h1>
        <h2 className="text-2xl">Jom belajar Next.JS!</h2>
      </div>

      <div className="px-3">
        <br />
        <h1 className=" decoration-double underline">
          see also other folder of basic nextjs routes
        </h1>
        <span>👇</span>
        <ul>
          <Link href={"/dynamic-routes"}>
            <li className="hover:underline">👉 [dynamic] routes</li>
          </Link>
          <Link href={"/catch-all"}>
            <li className="hover:underline">👉 [...catch-all] segments</li>
          </Link>
          <Link href={"/optional-catchall"}>
            <li className="hover:underline">
              👉 [[...optional]] catch-all segments
            </li>
          </Link>
        </ul>
        <p className="text-sm">
          The difference between catch-all and optional catch-all segments is
          that with optional, the route without the parameter is also matched
        </p>
      </div>
      <br />
    </main>
  );
}
