import { getData } from "@/services/products";
import Link from "next/link";
import React from "react";

export default async function DetailProductPage(props: any) {
  const { params } = props;
  //   const data = await getData(params.slug); sulg/id tu kena sama dgn nama folder cth dlm ini, [id]
  //   const product = await getData("http://localhost:3000/api/product/?id=" + params.id);
  const product = await getData(
    "https://fakestoreapi.com/products/" + params.id
  );
  console.log(product);

  return (
    <div className="container mx-auto my-10">
      <div className="w-1/2 mx-auto border border-gray-700">
        <img
          src={product.image}
          alt="alt"
          className="w-full object-cover aspect-square col-span-2"
        />
        <div className="bg-white p-4 px-6">
          <h3>{product.title}</h3>
          <p>Price: RM{product.price}</p>
          <p>😎 {product.description}</p>
        </div>
        <Link href={`/product`}>
          <button className="bg-gray-600 px-3 rounded text-white">back</button>
        </Link>
      </div>
    </div>
  );
}
