import Modal from "@/components/core/Modal";
import { getData } from "@/services/products";
import Link from "next/link";
import React from "react";

export default async function DetailProductPage(props: any) {
  const { params } = props;
  //   const data = await getData(params.slug); sulg/id tu kena sama dgn nama folder cth dlm ini, [id]
  //   const product = await getData("http://localhost:3000/api/product/?id=" + params.id);
  const product = await getData(
    "https://fakestoreapi.com/products/" + params.id
  );
  console.log(product);

  return (
    <Modal>
      <img
        src={product.image}
        alt="alt"
        className="w-full object-cover aspect-square col-span-2"
      />
      <div className="bg-white p-4 px-6">
        <h3>{product.title}</h3>
        <p>Price: RM{product.price}</p>
        <p>😎 {product.description}</p>
      </div>
    </Modal>
  );
}
