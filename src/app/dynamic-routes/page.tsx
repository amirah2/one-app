export default function DynamicRoutesPage() {
  return (
    <div className="px-5">
      <h1>My Sluggish List Page</h1>
      <ul className="list-disc">
        <li>
          url path: `/product/[slug]` → dynamic: [slug] became
          `product/testingggg` url or slug can be whatever you write in url path
          (params)
        </li>
        <li>
          contoh type kat url:{" "}
          <code>http://localhost:3000/dynamic-routes/hello-testing-okay</code>
          akan dibawa ke page yg lain tapi akan nampak slug-nya iaitu
          `hello-testing-okay`
        </li>
        <li>contoh sini: /dynamic-routes/[anythingg..]</li>
      </ul>
    </div>
  );
}
