// Dynamic Route: https://nextjs.org/docs/app/building-your-application/routing/dynamic-routes
// url path: `/product/[slug]` ==> dynamic: [slug] became product/testingggg or slug can be whatever you write in url path (params)

// 🟠 http://localhost:3000/sluggish/test_slug
// 🟠 Will display on screen Detail product page [slug]: test_slug

type DetailProductPageProps = { params: { slug: string } };

export default function Page(props: DetailProductPageProps) {
  const { params } = props;
  // console.log("params =>", params);

  return (
    <div className="px-3">
      <h1>Detail product page [slug]: </h1>
      <h2>{params.slug}</h2>
    </div>
  );
}

/**
Route	Example:
app/blog/[slug]/page.js
app/blog/[slug]/page.js
app/blog/[slug]/page.js
app/product/[slug]/page.tsx

URL:
/blog/a
/blog/b 
/blog/c
/product/hello

params:
{ slug: 'a' }
{ slug: 'b' }
{ slug: 'c' }
*/
