"use client";

import { useState } from "react";

export default function AdminProductPage() {
  const [status, setStatus] = useState("");

  const revalidate = async () => {
    console.log("revalidate button clicked");

    const res = await fetch(
      "http://localhost:3000/api/revalidate?tag=products&secret=12345678",
      {
        method: "POST",
      }
    );

    if (!res.ok) {
      setStatus("Revalidate Failed");
    } else {
      const response = await res.json();
      if (response.revalidate) {
        setStatus("Revalidate Success");
      }
    }

    // console.log(await res.json());
  };

  return (
    <div className=" w-3/6 h-96 bg-gray-700 rounded-[12px] flex justify-center items-center mr-5">
      <h1>Product Page</h1>

      <button
        className="ml-2 text-white bg-gray-600  rounded-sm p-3"
        onClick={() => revalidate()}
      >
        Revalidate
      </button>
      <h1 className="text-white">
        {status ? "Revalidate success!" : "Revalidate failed!"}
      </h1>
    </div>
  );
}
