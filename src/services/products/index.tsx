export const getData = async (url: string) => {
  // API dari luar, tapi kalau amik API dari api route sendiri?
  const res = await fetch(url, {
    cache: "no-store",
    next: {
      tags: ["products"],
    },
  });

  // API dari /api/product/route.ts
  // const res = await fetch("http://localhost:3000/api/product", {
  //   cache: "force-cache", // https://developer.mozilla.org/en-US/docs/Web/API/Request/cache
  //   next: {
  //     // revalidate: 30, // update setiap 30 saat. fungsi: utk performance (e.g. for analytics)
  //     tags: ["products"],
  //   },
  // });

  // console.log("response fakestore api:", res);

  if (!res.ok) {
    console.error("Failed to fetch data:", res.statusText);
    throw new Error("Failed to fetch data");
  }

  return res.json();
};
