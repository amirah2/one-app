// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
// boleh letak dalam .env
const firebaseConfig = {
  apiKey: "AIzaSyAPC2AXX5EvoBUj0r5frgtsyJXeTv3B5Oc",
  authDomain: "my-next-app-179d7.firebaseapp.com",
  projectId: "my-next-app-179d7",
  storageBucket: "my-next-app-179d7.appspot.com",
  messagingSenderId: "755908634898",
  appId: "1:755908634898:web:2a11568b3b9aa6171f2bcb",
  measurementId: "G-6EX37JB5B5",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

export { app };
